package javaspark;

import org.apache.spark.sql.SparkSession;

public interface SparkSessionWrapper {

    SparkSession spark = SparkSession
            .builder()
            .appName("Java Spark Examples")
            .master("local[*]")
            .getOrCreate();

}
