package javaspark.custom;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Custom transformations
 */
public class Transformations {

    public long myCounter(Dataset<Row> df) {
        return df.count();
    }

}
