package javaspark.exercise2sql;

import static org.apache.spark.sql.functions.*;

import java.util.Properties;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

public class FederatedLicencesSqlStats {
    public static final int COG2 = 0;
    public static final int FED2012 = 2;
    public static final int L_2012 = 3;
    public static final int POP_2010 = 36;
    public static final String FOOT = "111";
    public static final String dir = "src/main/java/javaspark/exercise2sql/";

    public static void main(String[] args) {
        SparkSession session = SparkSession.builder()
                .appName("exercise2sql.LicenceSqlStats")
                .master("local[*]")
                .config("spark.driver.bindAddress", "127.0.0.1")
                .getOrCreate();

        Dataset<Row> licences = session.read().format("csv")
                .option("header", "true")
                .option("delimiter", ";")
                .option("quote", "")
                .option("inferSchema", "true")
                .csv(dir + "licences_2012.csv");

        // read json to dataset
        Dataset<Row> feds = session.read().json(dir + "federations-et-codes.json");

        // join licences and federations
        Dataset<Row> results = licences.join(
                feds.withColumnRenamed("libelle", "nom"), col("fed_2012").equalTo(col("code")))
                .select(col("code"), col("nom"), col("l_2012"), col("pop_2010"))
                .groupBy(col("code"))
                .agg(first(col("nom"), true).alias("libelle"), sum(col("l_2012")).alias("total"))
                .orderBy(desc("total"))
                .limit(10);

        results.show(false);
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", "root");
        connectionProperties.put("password", "root");
        results
                .withColumnRenamed("fed_2012", "code")
                .withColumnRenamed("total", "nb")
                // save nb of licenses into a mysql table
                .write()
                .mode(SaveMode.Append)
                .jdbc("jdbc:mysql://localhost:3306/spk", "stat_licences", connectionProperties);

        session.close();
    }
}
