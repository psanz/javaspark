package javaspark.exercise2sql;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;
import scala.Tuple4;

public class LicenceStats {
	public static final int COG2 = 0;
	public static final int FED2012 = 2;
	public static final int L_2012 = 3;
	public static final int POP_2010 = 36;
	public static final String FOOT = "111"; 
	public static void main(String[] args) {
		SparkConf config = new SparkConf();
		config.setAppName("exercice2.LicenseStas");
		config.setMaster("local[*]");
		config.set("spark.driver.bindAddress", "127.0.0.1");
		JavaSparkContext context = new JavaSparkContext(config);

		// read data
		JavaRDD<String> lines = context.textFile("src/main/java/javaspark/exercise2sql/licences_2012.csv");
		// delete header line
		JavaPairRDD<String, Long> indexedLines = lines.zipWithIndex();
		JavaRDD<String> licenses = indexedLines.filter(p -> p._2 > 0).map(p -> p._1);
		// remove quotes
		JavaRDD<String> data = licenses.map(l -> l.substring(1, l.length() - 1));
		// select columns 0, 2, 3, 36
		JavaRDD<Tuple4<String, String, Integer, Integer>> info = data.map(d -> {
			String[] cells = d.split(";");
			String zipCode = cells[COG2].substring(0, 2);
			String fed2012 = cells[FED2012];
			Integer lic2012 = 0; 
			Integer pop2010 = 1; 
			lic2012= Integer.valueOf(cells[L_2012]);
			try {
				pop2010 = Integer.valueOf(cells[POP_2010]);
			} catch (Exception e) {
                // line may not have population stats
			}

			return new Tuple4<>(zipCode, fed2012, lic2012, pop2010);
			
		});
		
		// filter foot lines
		JavaPairRDD<String, Tuple2<Integer, Integer>> footInfo = 
				info.filter(q -> FOOT.equals(q._2() ) )
				.mapToPair(q -> new Tuple2<>(q._1(),
                        new Tuple2<>(q._3(), q._4())));
		
		// reduce on key to sum licenses and population
		JavaPairRDD<String, Tuple2<Integer, Integer>> footStats = 
				footInfo.reduceByKey((t1, t2) -> new Tuple2<>(t1._1 + t2._1, t1._2 + t2._2));
		JavaPairRDD<String, Double> footPerc = 
				footStats.mapToPair(fs ->
                        new Tuple2<>(fs._1, fs._2._1 * 100.0 / fs._2._2));
		
		footPerc.top(10, new PercComparator()).forEach(p -> System.out.println(p._1 + " -> " + p._2 + "%"));
		
		context.close();
	}
}
