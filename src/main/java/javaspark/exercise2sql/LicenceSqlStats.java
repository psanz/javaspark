package javaspark.exercise2sql;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.sum;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class LicenceSqlStats {
    public static final int COG2 = 0;
    public static final int FED2012 = 2;
    public static final int L_2012 = 3;
    public static final int POP_2010 = 36;
    public static final String FOOT = "111";

    public static void main(String[] args) {
        SparkSession session = SparkSession.builder()
                .appName("exercise2sql.LicenceSqlStats")
                .master("local[*]")
                .config("spark.driver.bindAddress", "127.0.0.1")
                .getOrCreate();

        Dataset<Row> licences = session.read().format("csv")
                .option("header", "true")
                .option("delimiter", ";")
                .option("quote", "")
                .option("inferSchema", "true")
                .csv("src/main/java/javaspark/exercise2sql/licences_2012.csv");

        // get 2 first chars of depts

        // 1st way: spark SQL api
        Dataset<Row> licDept = licences.withColumn("depts", licences.col("cog2").substr(2, 2));
        licDept
                .select("depts", "cog2", "fed_2012", "l_2012", "pop_2010")
                .filter("fed_2012 = '111'")
                .groupBy("depts")
                .agg(sum(col("l_2012")).multiply(100.0).divide(sum(col("pop_2010"))).alias("ratio"))
                .orderBy(col("ratio").desc())
                .limit(3).show(false);
        // 2nd way: with raw SQL query
        licences.createOrReplaceTempView("licences"); // register licences table so it's recognised by sql query
        Dataset<Row> licDept2 = session.sql(
                "select substring(cog2, 2, 2) as depts, sum(l_2012) * 100.0 / sum(pop_2010) as ratio"
                        + " from licences where fed_2012 = '111' group by depts order by ratio desc");
        licDept2
                .select("depts", "ratio")
                .limit(3).show();

        session.close();
    }
}
