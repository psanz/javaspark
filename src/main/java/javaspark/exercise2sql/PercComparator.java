package javaspark.exercise2sql;

import java.io.Serializable;
import java.util.Comparator;

import scala.Tuple2;

public class PercComparator implements Comparator<Tuple2<String, Double>>, Serializable {

	@Override
	public int compare(Tuple2<String, Double> t1, Tuple2<String, Double> t2) {
		return t1._2.compareTo(t2._2);
	}

}
