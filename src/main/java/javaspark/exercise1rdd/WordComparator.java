package javaspark.exercise1rdd;

import java.io.Serializable;
import java.util.Comparator;

import scala.Tuple2;

public class WordComparator implements Comparator<Tuple2<String, Integer>>, Serializable {

    @Override
    public int compare(Tuple2<String, Integer> t1, Tuple2<String, Integer> t2) {
        return t1._2.compareTo(t2._2);
    }

}
