package javaspark.exercise1rdd;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import javaspark.SparkSessionWrapper;
import scala.Tuple2;

public class AnagramCount implements SparkSessionWrapper {

    static Logger logger = Logger.getLogger(AnagramCount.class);

    public static void main(String[] args) {
        try (JavaSparkContext context = new JavaSparkContext(spark.sparkContext())) {

            // read data
            final String dir = "src/main/java/javaspark/exercise1rdd/";
            JavaRDD<String> lines = context.textFile(dir + "liste_mots_francais.txt");
            JavaRDD<String> words = lines.flatMap(l -> Arrays.asList(l.split(" ")).iterator());

            JavaPairRDD<String, String> keyWords =
                    words.mapToPair(w -> new Tuple2<>(orderChars(w), w));
            JavaPairRDD<String, Iterable<String>> wordsByAnagram = keyWords.groupByKey();
            JavaPairRDD<String, Iterable<String>> results = wordsByAnagram
                    .filter(wa -> wa._2.spliterator().getExactSizeIfKnown() > 1L);

//            results.foreach(r -> System.out.println(r.toString()));
            results.foreach(r -> logger.info(r));
        }
    }

    private static String orderChars(String input) {
        char[] charArray = input.toCharArray();
        Arrays.sort(charArray);
        return String.valueOf(charArray);
    }

}
