package javaspark.exercise1rdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.json.JSONArray;

import scala.Tuple2;

public class WordCount {

    public static void main(String[] args) {
        SparkConf config = new SparkConf();
        config.setAppName("exercice1.WordCount");
        config.setMaster("local[*]");
        //config.set("spark.driver.bindAddress", "127.0.0.1");
        JavaSparkContext context = new JavaSparkContext(config);

        // read stop-words
        final String dir = "src/main/java/javaspark/exercise1rdd/";
        JavaRDD<String> stopLines = context.textFile(dir + "stop-words-en.json");
        JavaRDD<String> stopWords = stopLines.flatMap(sl -> {
            JSONArray array = new JSONArray(sl);
            List<String> list = new ArrayList<>();
            array.forEach(s -> list.add((String) s));
            return list.iterator();
        });

        // read data
        JavaRDD<String> lines = context.textFile(dir + "sherlock.txt");
        JavaRDD<String> words = lines.flatMap(l -> Arrays.asList(l.split(" ")).iterator());
        JavaRDD<String> goodWords = words.subtract(stopWords);
        JavaPairRDD<String, Integer> counters =
                goodWords.mapToPair(w -> new Tuple2<>(w, 1));
        JavaPairRDD<String, Integer> results = counters.reduceByKey(Integer::sum);

        results.top(50, new WordComparator()).forEach(System.out::println);

        context.close();
    }

}
