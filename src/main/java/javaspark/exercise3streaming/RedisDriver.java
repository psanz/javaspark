package javaspark.exercise3streaming;

import java.io.Serializable;

import redis.clients.jedis.Jedis;

public class RedisDriver extends Jedis implements Serializable {

	public RedisDriver(String host) {
		super(host);
	}

	public RedisDriver(String host, int port) {
		super(host, port);
	}

}
