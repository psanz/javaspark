package javaspark.exercise3streaming;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.flume.FlumeUtils;
import org.apache.spark.streaming.flume.SparkFlumeEvent;

import scala.Tuple2;

public class StreamingTest {

    protected static Logger ex3log = Logger.getLogger(StreamingTest.class);

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setAppName("exercise3streaming.StreamingTest")
                .setMaster("local[*]");
        conf.set("spark.driver.bindAddress", "127.0.0.1");
        JavaStreamingContext context = new JavaStreamingContext(conf, new Duration(5000));
        // define receiver
        final String HOST = "localhost";
        final int PORT = 4444;
        final long TIMEOUT = 500000;
        JavaReceiverInputDStream<SparkFlumeEvent> receiver = FlumeUtils.createStream(context, HOST, PORT);

        JavaDStream<String> lines = receiver.map(fEvent -> new String(fEvent.event().getBody().array()));
        JavaPairDStream<String, Integer> counters = lines.flatMap(line -> Arrays.asList(line.split(" ")).iterator())
                .mapToPair(word -> new Tuple2<>(word, 1));

        JavaPairDStream<String, Integer> results = counters.reduceByKey((i1, i2) -> i1 + i2);
        // results.foreachRDD(rdd -> rdd.foreach(pair -> );
        RedisDriver connection = new RedisDriver("localhost", 6379);
        results.foreachRDD(rdd -> rdd.foreach(pair -> {
            System.out.println(pair._1 + " => " + pair._2);
            connection.incrBy(pair._1, pair._2);
        }));
        context.start();

        try {
            context.awaitTerminationOrTimeout(TIMEOUT);
        } catch (Exception e) {
        } finally {
            context.close();
            context.stop();
            connection.close();
        }

    }

}
