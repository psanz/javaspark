package javaspark.custom;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Test;

import javaspark.SparkSessionWrapper;

/**
 * Custom DataSet transformations
 */
public class TransformationsTest implements SparkSessionWrapper {

    @Test
    public void testMyCounter() {
        try (JavaSparkContext sparkContext = new JavaSparkContext(spark.sparkContext())) {

            List<String[]> stringAsList = new ArrayList<>();
            stringAsList.add(new String[] { "cell-1.1", "cell-2.1" });
            stringAsList.add(new String[] { "cell-1.2", "cell-2.2" });

            JavaRDD<Row> rowRDD = sparkContext.parallelize(stringAsList).map(RowFactory::create);

            // Creates schema
            StructType schema = DataTypes
                    .createStructType(
                            new StructField[] { DataTypes.createStructField("col-1", DataTypes.StringType, false),
                                    DataTypes.createStructField("col-2", DataTypes.StringType, false) });

            Dataset<Row> dataset = spark.sqlContext().createDataFrame(rowRDD, schema).toDF();

            dataset.show(false);

            Transformations transformations = new Transformations();
            long result = transformations.myCounter(dataset);
            assertEquals(2, result);
        }
    }

}
