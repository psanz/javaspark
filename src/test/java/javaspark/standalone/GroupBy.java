package javaspark.standalone;

import static org.apache.spark.sql.functions.*;

import java.util.ArrayList;
import java.util.List;

import javaspark.SparkSessionWrapper;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.junit.Test;

import javaspark.standalone.model.Employee;

/**
 * Standalone tests on DataSet GroupBy
 */
public class GroupBy implements SparkSessionWrapper {

    public static final String SALES = "Sales";
    public static final String FINANCE = "Finance";
    public static final String MARKETING = "Marketing";
    public static final String NY = "NY";
    public static final String CA = "CA";

    @Test
    public void testMyCounter() {

        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("James", SALES, NY, 90000, 34, 10000));
        employees.add(new Employee("James", SALES, NY, 90000, 34, 10000));
        employees.add(new Employee("Michael", SALES, NY, 86000, 56, 20000));
        employees.add(new Employee("Robert", SALES, CA, 81000, 30, 23000));
        employees.add(new Employee("Maria", FINANCE, CA, 90000, 24, 23000));
        employees.add(new Employee("Raman", FINANCE, CA, 99000, 40, 10000));
        employees.add(new Employee("Scott", FINANCE, NY, 83000, 36, 19000));
        employees.add(new Employee("Jen", FINANCE, NY, 79000, 53, 15000));
        employees.add(new Employee("Jeff", MARKETING, CA, 80000, 25, 18000));
        employees.add(new Employee("Kumar", MARKETING, NY, 91000, 50, 21000));

        try (JavaSparkContext sparkContext = new JavaSparkContext(spark.sparkContext())) {
            sparkContext.setLogLevel("WARN");

            Encoder<Employee> employeeEncoder = Encoders.bean(Employee.class);
            Dataset<Employee> employeeDs = spark.createDataset(employees, employeeEncoder);

            // add new column ageSalaryRatio = salary/age
            Dataset<Row> rows = employeeDs
                    .withColumn("ageSalaryRatio", employeeDs.col("salary").divide(employeeDs.col("age")));
            rows.show();

            final Dataset<Row> deptStats = rows
                    .filter(col("salary").$greater(30))
                    // filter employees in state starting by N or C
                    .filter(substring(col("state"), 0, 1).isin("N", "C"))
                    .groupBy("department", "state")
                    .agg(
                            sum("salary").as("sum_salary"),
                            avg("salary").as("avg_salary"),
                            sum("bonus").as("sum_bonus"),
                            max("bonus").as("max_bonus"),
                            avg(col("salary").plus(col("bonus"))).as("avg_salary_plus_bonus"),
                            avg("ageSalaryRatio").as("avg_ageSalaryRatio"));
            deptStats.show(false);

            Dataset<Row> employeeExtremes = rows.agg(
                    min("salary").as("min_salary"),
                    max("salary").as("max_salary"),
                    min("bonus").as("min_bonus"),
                    max("bonus").as("max_bonus"),
                    min(col("salary").plus(col("bonus"))).as("min_salary_plus_bonus"),
                    max(col("salary").plus(col("bonus"))).as("max_salary_plus_bonus")
            );
            employeeExtremes.show(false);

        }
    }

}
