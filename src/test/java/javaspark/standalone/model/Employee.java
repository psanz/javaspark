package javaspark.standalone.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Employee {
    @NonNull
    String employee_name;
    @NonNull
    String department;
    @NonNull
    String state;
    @NonNull
    Integer salary;
    @NonNull
    Integer age;
    @NonNull
    Integer bonus;
}
