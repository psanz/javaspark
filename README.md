# JavaSpark

Java 8 + Spark examples

## Content

I'm just starting.

In the main module, you'll find some basic custom actions, and some exercises for rdd, sql and streaming done during a training. 

However, I'm planning to start including examples directly with junit.

So far, the new tests are in:
src/test/java/javaspark/standalone

Enjoy!

## Todo

* Add custom DataSet transformation
* Add Dataset Pivot example